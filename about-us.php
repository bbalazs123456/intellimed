<section id="about-us" class="about-us content">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <img src="components/img/blood-care.png" alt="">
        </div>
        <div class="col-md-6">
          <h1 class="animated SlideInUp">A Társaság céljai, tevékenysége</h1>
          <hr>
          <span class="animated SlideInUp">A Társaság elősegíti a tagok szakmai és tudományos tevékenységét, az orvostudomány és ezen belül különösen a Hematológia és Transzfúziológia területeivel foglalkozó tudományág fejlesztését.
          Saját szakterületén elősegíti az országos szintű tudományos, oktatási, etikai és társadalmi feladatok megoldását.
          A célok megvalósítása érdekében folytatott tevékenységet, a tagság jogait és kötelezettségeit a Társaság Alapszabályzata tartalmazza. </span>
          <a href="/subpages/management.php"><h3>Kattintson a vezetői tagokhoz ...</h3></a>
        </div>
      </div>
    </div>
  </div>
</section>
