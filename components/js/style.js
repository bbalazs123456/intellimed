var modal = document.getElementById('simpleModal');
var modalBtn = document.getElementById('modalBtn');
var closeBtn = document.getElementById('closeBtn');
var succesOk = document.getElementById('registeredSucces');
var cancelCancel = document.getElementById('registeredCancel');

// esemény
modalBtn.addEventListener('click', openModal);
closeBtn.addEventListener('click', closeModal);
window.addEventListener('click', clickOut);
succesOk.addEventListener('click', registered);
cancelCancel.addEventListener('click', canceled);

// open modal esemény

function openModal(){
  modal.style.display = 'block';
}
// close  modal esemény
function closeModal(){
  modal.style.display = 'none';
}
//close ha bárhova kattintasz
function clickOut(e){
  if(e.target == modal){
    modal.style.display = 'none';
  }
}
function registered(){
  modal.style.display = 'none';
}
function canceled(){
  modal.style.display = 'none';
}
