<div id="map">

</div>

<footer id="information">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
          <h3>Hasznos linkek</h3>
          <a href="#">Adatvédelem</a>
          <a href="#">Súgó</a>

        </div>
        <div class="col-md-4">
          <h3>Elérhetőségeink</h3>
          <span>Magyar Hematológiai és Transzfuziológiai Társaság
              Szent László Kórház, Hematológiai Osztály
              1097 Budapest, Gyáli út 5-7.</span>
        </div>
        <div class="col-md-4">
          <h3>Üzemeltető</h3>
          <a target="_blank" href="http://www.intellimed.eu/"><img class="logo" src="<?= $base_url ?>components/img/intellimed-logo.svg" alt=""></a>
        </div>
      </div>
    </div>
  </div>
</footer>
