<?php
$url_ext = $_SERVER['SERVER_NAME'] == 'localhost' ? ':8080' : '';
$base_url = 'http://' . $_SERVER['SERVER_NAME'].$url_ext.'/';
?>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>	Magyar Hematológiai és Transzfuziológiai Társaság On-line</title>
<link href="http://www.intellimed.eu/templates/intellimed/favicon.ico" rel="shortcut icon"/>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= $base_url ?>components/less/style.css">
<link rel="stylesheet" type="text/css" href="<?= $base_url ?>components/less/responsive.css">
<link rel="stylesheet" type="text/css" href="<?= $base_url ?>components/css/animate.css-master/animate.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?= $base_url ?>components/js/style.js"></script>

<script>

// google map api
function initMap() {

    var location = {lat: 47.47555, lng: 19.0900273};
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 15, center: location});
    var marker = new google.maps.Marker({position: location, map: map});
}
</script>

<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF1WtNX40vwNeDRWh7Bb0SE0lcduWBX9Q &callback=initMap">
</script>