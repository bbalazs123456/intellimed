<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include 'head.php' ?>
  </head>
  <body>

    <div class="nav-opening-title animated fadeIn">
      <div class="container">
        <div class="row">
          <h1 class="animated bounceInRight">MAGYAR HEMATOLÓGIAI ÉS TRANSZFÚZIOLÓGIAI TÁRSASÁG</h1>
          <hr class="animated fadeIn">
          <span class="animated bounceInLeft">HUNGARIAN SOCIETY OF HAEMATOLOGY AND TRANSFUSION</span>
        </div>
      </div>
    </div>

    <?php include 'header.php' ?>

    <?php include 'about-us.php' ?>

    <div id="myCarousel" class="carousel slide myCarousel" data-interval="false" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <!-- Carousel items -->
      <div class="carousel-inner">
        <div class="active item slider1">
          <!--put img inside itens like this-->
            <div class="container">
              <div class="carousel-caption ">
                <h1 style="animation-delay: 1s;"class="animated bounceInRight">Hematológiai és Transzfúziológia c. folyóirat </h1>
                <h3 style="animation-delay: 2s;" class="animated bounceInLeft">Új szerzői útmutató</h3>
            </div>
            </div>
        </div>
        <div class="item slider2">
            <div class="container">
            <div class="carousel-caption">
              <h1 style="animation-delay: 1s;" class="animated bounceInRight">GDPR -Fontos felhívás tagjaink számára!</h1>
              <h3 style="animation-delay: 2s;" class="animated bounceInLeft">Az Európai Unió Egységes Adatvédelmi Szabályozást (GDPR - General Data Protection Regulation) vezetett be 2018. május 26-tól.
                Kérjük, a honlapra belépve nyilatkozzon adatai kezeléséről!</h3>
            </div>
            </div>
        </div>
        <div class="item slider3">
            <div class="container">
            <div class="carousel-caption">
              <h3 style="animation-delay: 1s;" class="animated bounceInRight">Lorem ipsum dolor sit amet</h3>
            </div>
            </div>
        </div>
      </div>
      <hr class="transition-timer-carousel-progress-bar" />
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <section id="news" class="news-block">
      <div class="container">
        <div class="row">
          <h1>Friss Hírek</h1>
          <hr>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>V. Molekuláris Hematológia Konferencia (Kaposvár, 2019.02.28-03.02.)</h4>
                </div>

                <div class="img-block img-bg-1">

                </div>

                <hr>

                <div class="lead-block">
                  A Magyar Hematológiai és Transzfúziológiai Társaság és a Somogy Megyei Kaposi Mór Oktató Kórház az V. Molekuláris Hematológia Konferenciát 2019. február 28 - március 2. között Kaposváron rendezi meg, amelyre szeretettel meghívjuk.
                </div>

                <a href="subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>GDPR - Fontos felhívás tagjaink számára!</h4>
                </div>

                <div class="img-block img-bg-2">
                </div>

                <hr>

                <div class="lead-block">
                  A honlapon regisztrált felhasználók korábban jóváhagyásukat adták, hogy megadott adataikat a Magyar Hematológiai és Transzfuziológiai Társaság kizárólag a saját működése, működtetése, valamint szakmai rendezvényekről történő tájékoztatás, illetve egyéb szakmai információk, szakmai ...
                </div>

                <a href="subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>Budapesti Immunológiai Fórum (2019.02.07-09.)</h4>
                </div>

                <div class="img-block img-bg-3">
                </div>

                <hr>

                <div class="lead-block">
                  Tisztelt Kollégák!

                  Tisztelt Kollégák! Tisztelt Kollégák! 2009-ben indítottuk útjára a kétévente megrendezésre kerülő Budapesti Immunológiai Fórumot (BIF). A BIF tanfolyamok célja a klinikai immunológia megértéséhez nélkülözhetetlen immunológiai alapfogalmak és az ...
                </div>

                <a href="subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>V. Molekuláris Hematológia Konferencia (Kaposvár, 2019.02.28-03.02.)</h4>
                </div>

                <div class="img-block img-bg-3">

                </div>

                <hr>

                <div class="lead-block">
                  A Magyar Hematológiai és Transzfúziológiai Társaság és a Somogy Megyei Kaposi Mór Oktató Kórház az V. Molekuláris Hematológia Konferenciát 2019. február 28 - március 2. között Kaposváron rendezi meg, amelyre szeretettel meghívjuk.
                </div>

                <a href="subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>V. Molekuláris Hematológia Konferencia (Kaposvár, 2019.02.28-03.02.)</h4>
                </div>

                <div class="img-block img-bg-1">
                </div>

                <hr>

                <div class="lead-block">
                  A Magyar Hematológiai és Transzfúziológiai Társaság és a Somogy Megyei Kaposi Mór Oktató Kórház az V. Molekuláris Hematológia Konferenciát 2019. február 28 - március 2. között Kaposváron rendezi meg, amelyre szeretettel meghívjuk.
                </div>

                <a href="./MHTT/MHTT/subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

              <div class="col-md-4 container-blocks">

                <div class="title-block">
                  <h4>V. Molekuláris Hematológia Konferencia (Kaposvár, 2019.02.28-03.02.)</h4>
                </div>

                <div class="img-block img-bg-1">
                </div>

                <hr>

                <div class="lead-block">
                  A Magyar Hematológiai és Transzfúziológiai Társaság és a Somogy Megyei Kaposi Mór Oktató Kórház az V. Molekuláris Hematológia Konferenciát 2019. február 28 - március 2. között Kaposváron rendezi meg, amelyre szeretettel meghívjuk.
                </div>

                <a href="./MHTT/subpages/inprogress.php"><span>Tovább...</span></a>

              </div>

            </div>
          </div>
        </div>
      </div>
    </section>

	<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

	<?php include 'partners.php' ?>
    <?php include 'footer.php' ?>

  </body>

  <script type="text/javascript">

    //scroll-to

    $(".scroll-to").click(function (e) {
      e.preventDefault;
      var target = $(this).attr('href');
      $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
    });

	// ===== Scroll to Top ==== 
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
				$('#return-to-top').fadeIn(200);    // Fade in the arrow
			} else {
				$('#return-to-top').fadeOut(200);   // Else fade out the arrow
			}
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
			$('body,html').animate({
				scrollTop : 0                       // Scroll to top of body
			}, 500);
		});

    //sticky

    window.onscroll = function() {myFunction()};

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }


    $(document).ready(function(){
      $('.your-class').slick({
                dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
      });
    });

    // slider bar

    $(document).ready(function(){
    	var percent = 0,
    	interval = 30,//it takes about 6s, interval=20 takes about 4s
    	$bar = $('.transition-timer-carousel-progress-bar'),
    	$crsl = $('#myCarousel');
    	$('.carousel-indicators li, .carousel-control').click(function (){$bar.css({width:0.5+'%'});});
    	/*line above just for showing when controls are clicked the bar goes to 0.5% to make more friendly,
    	if you want when clicked set bar empty, change on width:0.5 to width:0*/
    	$crsl.carousel({//initialize
    		interval: false,
    		pause: true
    	}).on('slide.bs.carousel', function (){percent = 0;});//This event fires immediately when the bootstrap slide instance method is invoked.
    	function progressBarCarousel() {
    		$bar.css({width:percent+'%'});
    		percent = percent +0.5;
    		if (percent>=100) {
    			percent=0;
    			$crsl.carousel('next');
    		}
    	}
    	var barInterval = setInterval(progressBarCarousel, interval);//set interval to progressBarCarousel function
    	if (!(/Mobi/.test(navigator.userAgent))) {//tests if it isn't mobile
    		$crsl.hover(function(){
    					clearInterval(barInterval);
    				},
    				function(){
    					barInterval = setInterval(progressBarCarousel, interval);
    				}
    		);
    	}
    });

  </script>
  <script src="<?= $base_url ?>components/js/style.js"></script>

</html>
