    <section id="partners" class="partners">
      <div class="container">
        <div class="row">
          <div class="partner-blocks">
            <h1>Partnereink</h1>
            <hr>

            <div class="col-md-12">
              <div class="row">
                <div class="col-md-4 col-sm-3">
                  <a target="_blank" href="https://www.celgene.com/"><img src="<?= $base_url ?>components/img/logo1.jpg" alt=""></a>
                </div>
                <div class="col-md-4 col-sm-3">
                  <a target="_blank" href="http://roche.hu"><img src="<?= $base_url ?>components/img/logo2.jpg" alt=""></a>
                </div>
                <div class="col-md-4 col-sm-3">
                  <a target="_blank" href="http://www.janssen-cilag.hu/"><img src="<?= $base_url ?>components/img/logo3.jpg" alt=""></a>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </section>