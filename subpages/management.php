<?php include '../head.php' ?>
<?php include '../header.php' ?>

<section class="bg-block animated fadeIn">
  <div class="container">

    <div class="management-block">

        <div class="row">

          <div class="col-md-9">

            <h1>A Társaság vezető szervei, bizottságai</h1>

            <span>A tisztújító közgyűlés 2017. május 19-én megválasztotta az MHTT új vezetőségét: </span>

            <hr style="margin-bottom: 80px;">

            <div style="animation-delay: 1.5s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="<?= $base_url ?>components/img/person1.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Dr. Illés Árpád</h3>
                <span>Elnök</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 2s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="<?= $base_url ?>components/img/person2.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Dr. Vörös Katalin </h3>
                <span>Főtitkár</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 2.5s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="<?= $base_url ?>components/img/person4.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Dr. Vezendi Klára </h3>
                <span>Jegyző</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

            <div style="animation-delay: 3s;" class="persons-container animated fadeInUp">
              <div class="col-md-2">
                <img src="<?= $base_url ?>components/img/person3.png" alt="">
              </div>
              <div class="col-md-10">
                <h3>Dr. Gurzó Mihály </h3>
                <span>pénztáros</span>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>

          </div>
          <div style="animation-delay: 4s;" class="col-md-3 animation animated fadeInUp">
            <div class="calendar">
              <h3>Eseménynaptár</h3>
            </div>

			<div class="img-section">
				<img src="<?= $base_url ?>components/img/woman.jpg" alt="">
			</div>

			<div class="credit-account-number">
				<h3>Bankszámla szám</h3>

				<hr>

				<span><strong>Éves tagsági díj:</strong> 1.000.- Ft.Ezt minden évben átutalással vagy csekken kérjük befizetni. </span>

				<span><strong>Jogosult neve:</strong> Magyar Hematológiai és Transzfúziológiai Társaság. 
Számlaszám: 11732002-20372943-00000000 </span>

				<span>Az átutalás megjegyzése tartalmazza a tag nevét és azt, hogy "<strong>x évi tagdíj</strong>".</span>
			</div>

          </div>
          
        </div>

    </div>

  </div>
</section>

<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

<?php include '../partners.php' ?>
<?php include '../footer.php' ?>

<script>

    //scroll-to

    $(".scroll-to").click(function (e) {
      e.preventDefault;
      var target = $(this).attr('href');
      $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 1000);
    });

	// ===== Scroll to Top ==== 
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
				$('#return-to-top').fadeIn(200);    // Fade in the arrow
			} else {
				$('#return-to-top').fadeOut(200);   // Else fade out the arrow
			}
		});
		$('#return-to-top').click(function() {      // When arrow is clicked
			$('body,html').animate({
				scrollTop : 0                       // Scroll to top of body
			}, 500);
		})
</script>

<script src="<?= $base_url ?>components/js/style.js"></script>